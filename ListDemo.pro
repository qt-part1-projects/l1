#-------------------------------------------------
#
# Project created by QtCreator 2016-02-12T10:19:46
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = ListDemo
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
